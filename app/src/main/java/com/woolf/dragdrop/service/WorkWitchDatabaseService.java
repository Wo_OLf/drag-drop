package com.woolf.dragdrop.service;

import android.app.Service;
import android.content.Intent;
import android.os.IBinder;
import android.support.annotation.Nullable;

import com.woolf.dragdrop.bd.ItemDatabaseHelper;
import com.woolf.dragdrop.data.Action;
import com.woolf.dragdrop.data.DragAndDropData;

import java.util.ArrayList;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * Created by WoOLf on 24.01.2016.
 */
public class WorkWitchDatabaseService extends Service {

    ExecutorService mExecutorService;
    ItemDatabaseHelper mItemDatabaseHelper = ItemDatabaseHelper.getInstance(this);

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        mExecutorService = Executors.newSingleThreadExecutor();
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        int action = intent.getExtras().getInt(Action.KEY_ACTION);
        switch (action) {
            case Action.GET_FIRST_RECORD_FROM_DB:
                mExecutorService.execute(new GetFirstRecord(startId));
                break;
            case Action.LOAD_FROM_DB:
                int firstId = intent.getExtras().getInt(Action.KEY_FIRST_ID);
                mExecutorService.execute(new LoadFromDatabase(firstId,startId));
                break;

            case Action.FILL_DB:
                mExecutorService.execute(new GenerateList(startId));
                break;

            case Action.UPDATE_DB:
                int fromId = intent.getExtras().getInt(Action.KEY_FROM_ID);
                int toId = intent.getExtras().getInt(Action.KEY_TO_ID);
                boolean isMovedUp = intent.getExtras().getBoolean(Action.KEY_IS_MOVED_UP);
                mExecutorService.execute(new UpdateDatabase(startId,fromId,toId,isMovedUp));
                break;
        }
        return START_NOT_STICKY;
    }

    @Override
    public void onDestroy() {
        mExecutorService.shutdownNow();
        ItemDatabaseHelper.destroy();
        super.onDestroy();
    }


    class UpdateDatabase implements Runnable{

        int startId;
        int fromId;
        int toId;
        boolean isMovedUp;

        public UpdateDatabase(int startId, int fromId, int toId, boolean isMovedUp) {
            this.startId = startId;
            this.fromId = fromId;
            this.toId = toId;
            this.isMovedUp = isMovedUp;
        }

        @Override
        public void run() {
            boolean isSuccess = mItemDatabaseHelper.updateDatabase(fromId,toId,isMovedUp);
            Intent intent = new Intent(Action.BROADCAST_ACTION);
            intent.putExtra(Action.KEY_ACTION, Action.UPDATE_DB);
            intent.putExtra(Action.KEY_STATUS, isSuccess);
            sendBroadcast(intent);
            stopSelf(startId);

        }
    }

    class LoadFromDatabase implements Runnable {
        int startId;
        int firstId;

        public LoadFromDatabase(int firstId,int startId) {
            this.startId = startId;
            this.firstId = firstId;
        }

        @Override
        public void run() {
            ArrayList<DragAndDropData> list = mItemDatabaseHelper.loadFromDatabase(firstId);
            Intent intent = new Intent(Action.BROADCAST_ACTION);
            intent.putExtra(Action.KEY_ACTION, Action.LOAD_FROM_DB);
            intent.putExtra(Action.KEY_LIST, list);
            sendBroadcast(intent);
            stopSelf(startId);
        }
    }

    class GenerateList implements Runnable {
        int startId;

        public GenerateList(int startId) {
            this.startId = startId;
        }

        @Override
        public void run() {
            boolean isSuccess = mItemDatabaseHelper.fillDatabase();
            Intent intent = new Intent(Action.BROADCAST_ACTION);
            intent.putExtra(Action.KEY_ACTION, Action.FILL_DB);
            intent.putExtra(Action.KEY_STATUS, isSuccess);
            sendBroadcast(intent);
            stopSelf(startId);
        }
    }


    class GetFirstRecord implements Runnable {
        int startId;

        public GetFirstRecord(int startId) {
            this.startId = startId;
        }

        @Override
        public void run() {
            int firstId = mItemDatabaseHelper.getHeadRecord();
            Intent intent = new Intent(Action.BROADCAST_ACTION);
            intent.putExtra(Action.KEY_ACTION, Action.GET_FIRST_RECORD_FROM_DB);
            intent.putExtra(Action.KEY_FIRST_ID, firstId);
            sendBroadcast(intent);
            stopSelf(startId);
        }
    }

}

package com.woolf.dragdrop.bd;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.woolf.dragdrop.data.DragAndDropData;
import com.woolf.dragdrop.data.DragAndDropDataHelper;

import java.util.ArrayList;

/**
 * Created by WoOLf on 25.01.2016.
 */
public class ItemDatabaseManager implements DatabaseKeys {

    /**
     * Метод заполняет базу данные если в ней нет записей
     *
     * @param databaseHelper
     * @return true - если база успешно заполнена ,false - ошибка во время заполнения
     */

    public static boolean fillDatabase(ItemDatabaseHelper databaseHelper) {
        SQLiteDatabase db = databaseHelper.getWritableDatabase();
        long prev_id = 0;
        long current_id;
        try {
            db.beginTransaction();
            ContentValues contentValues = new ContentValues();

            for (String value : getValuesList()) {
                current_id = addNewRecord(db, prev_id, contentValues, value);
                contentValues.clear();

                if (prev_id != 0) {
                    updateRecord(db, prev_id, current_id, contentValues);
                    contentValues.clear();
                }

                prev_id = current_id;

            }
            db.setTransactionSuccessful();
        } catch (Exception e) {
            return false;
        } finally {
            db.endTransaction();
            databaseHelper.close();
        }
        return true;
    }

    private static ArrayList<String> getValuesList() {
        ArrayList<String> list = new ArrayList<>();
        for (int i = 0; i < 1000; i++) {
            list.add("Value " + (i + 1));
        }
        return list;
    }

    /**
     * Получение 1го элемнта списка
     *
     * @param databaseHelper
     * @return {@code != 0} id первой записи в бд
     * {@code 0}   в бд нет записей
     */

    public static int getIdFirsRecord(ItemDatabaseHelper databaseHelper) {
        SQLiteDatabase db = databaseHelper.getWritableDatabase();
        String selection = " previous_id IS NULL";
        int recordId;
        Cursor cursor = db.query(ITEMS_TABLE, null, selection, null, null, null, null);
        if (cursor.moveToFirst()) {
            int id = cursor.getColumnIndex(_ID);
            recordId = cursor.getInt(id);
        } else {
            recordId = 0;
        }
        cursor.close();
        databaseHelper.close();
        return recordId;
    }

    /**
     * Заполнение списка из бд
     *
     * @param firstRecordId
     * @param databaseHelper
     * @return
     */

    public static ArrayList<DragAndDropData> fillListFromDataBase(int firstRecordId, ItemDatabaseHelper databaseHelper) {
        SQLiteDatabase db = databaseHelper.getWritableDatabase();
        ArrayList<DragAndDropData> list = new ArrayList<>();
        try {
            db.beginTransaction();
            fill(db, firstRecordId, list);
            db.setTransactionSuccessful();
        } catch (Exception e) {
            return null;
        } finally {
            db.endTransaction();
            databaseHelper.close();
        }


        return list;
    }

    /**
     * @param fromId
     * @param toId
     * @param isMovedUp
     * @param databaseHelper
     * @return
     */

    public static boolean updateDatabase(int fromId, int toId, boolean isMovedUp, ItemDatabaseHelper databaseHelper) {
        SQLiteDatabase db = databaseHelper.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        DragAndDropDataHelper fromData;
        DragAndDropDataHelper toData;
        try {
            db.beginTransaction();

            fromData = getDataById(db, fromId);
            toData = getDataById(db, toId);
            if (fromData == null || toData == null) {
                return false;
            }
            //remove
            addNullValue(NEXT_ID, contentValues);
            addNullValue(PREVIOUS_ID, contentValues);
            updateRecord(db, fromId, contentValues);

            addContentValues(NEXT_ID, fromData.getNextId(), contentValues);
            updateRecord(db, fromData.getPreviousId(), contentValues);

            addContentValues(PREVIOUS_ID, fromData.getPreviousId(), contentValues);
            updateRecord(db, fromData.getNextId(), contentValues);

            if (isMovedUp) {
                addContentValues(NEXT_ID, toId, contentValues);
                addContentValues(PREVIOUS_ID, toData.getPreviousId(), contentValues);
            } else {
                addContentValues(NEXT_ID, toData.getNextId(), contentValues);
                addContentValues(PREVIOUS_ID, toId, contentValues);
            }
            updateRecord(db, fromId, contentValues);

            addContentValues(NEXT_ID, fromId, contentValues);
            if (isMovedUp) {
                updateRecord(db, toData.getPreviousId(), contentValues);
            } else {
                updateRecord(db, toId, contentValues);
            }

            addContentValues(PREVIOUS_ID, fromId, contentValues);
            if (isMovedUp) {
                updateRecord(db, toId, contentValues);
            } else {
                updateRecord(db, toData.getNextId(), contentValues);
            }

            db.setTransactionSuccessful();
        } catch (Exception e) {
            return false;
        } finally {
            db.endTransaction();
            databaseHelper.close();
        }

        return true;

    }

    public static void addContentValues(String key, int value, ContentValues contentValues) {
        if (value == 0) {
            contentValues.putNull(key);
        } else {
            contentValues.put(key, value);
        }
    }

    public static void addNullValue(String key, ContentValues contentValues) {
        contentValues.putNull(key);
    }

    public static void updateRecord(SQLiteDatabase db, int id, ContentValues contentValues) {
        if (id != 0) {
            db.update(ITEMS_TABLE, contentValues, "_id = ?", new String[]{String.valueOf(id)});
        }
        contentValues.clear();

    }

    private static DragAndDropDataHelper getDataById(SQLiteDatabase db, int id) {
        Cursor cursor = db.query(ITEMS_TABLE, null, "_id = " + id, null, null, null, null);
        if (cursor.moveToFirst()) {
            DragAndDropDataHelper data = parseCursor(cursor);
            cursor.close();
            return data;
        } else return null;
    }

    private static void fill(SQLiteDatabase db, int id, ArrayList<DragAndDropData> linkedList) {
        Cursor cursor = db.query(ITEMS_TABLE, null, " _id = " + id, null, null, null, null);
        if (cursor.moveToFirst()) {
            DragAndDropDataHelper data = parseCursor(cursor);
            cursor.close();
            if (data != null) {
                linkedList.add(data.getData());
                if (data.getNextId() != 0) {
                    fill(db, data.getNextId(), linkedList);
                }
            }
        }
    }

    private static DragAndDropDataHelper parseCursor(Cursor cursor) {
        if (cursor.moveToFirst()) {
            int idIndex = cursor.getColumnIndex(_ID);
            int valueIndex = cursor.getColumnIndex(VALUE);
            int previousIdIndex = cursor.getColumnIndex(PREVIOUS_ID);
            int nextIdIndex = cursor.getColumnIndex(NEXT_ID);

            int id = cursor.getInt(idIndex);
            String value = cursor.getString(valueIndex);
            int previous_id = cursor.getInt(previousIdIndex);
            int next_id = cursor.getInt(nextIdIndex);
            return new DragAndDropDataHelper(new DragAndDropData(id, value), previous_id, next_id);
        } else return null;
    }


    private static long addNewRecord(SQLiteDatabase db, long prev_id, ContentValues contentValues, String value) {
        contentValues.put(VALUE, value);
        if (prev_id != 0) {
            contentValues.put(PREVIOUS_ID, prev_id);
        }
        return db.insert(ITEMS_TABLE, null, contentValues);
    }

    private static void updateRecord(SQLiteDatabase db, long prev_id, long current_id, ContentValues contentValues) {
        contentValues.put(NEXT_ID, current_id);
        db.update(ITEMS_TABLE, contentValues, "_id = ?", new String[]{String.valueOf(prev_id)});
    }

}

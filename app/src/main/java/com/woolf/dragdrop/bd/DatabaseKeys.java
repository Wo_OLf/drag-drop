package com.woolf.dragdrop.bd;

import android.provider.BaseColumns;

/**
 * Created by WoOLf on 21.01.2016.
 */
public interface DatabaseKeys extends BaseColumns {
    String ITEMS_TABLE = "items_table";

    String VALUE = "value";
    String PREVIOUS_ID = "previous_id";
    String NEXT_ID = "next_id";


    String CREATE_ITEMS_TABLE = String.format(
            "CREATE TABLE %1$s (" +                                         //ITEMS_TABLE
                    " %2$s INTEGER PRIMARY KEY  AUTOINCREMENT" +            //_ID
                    ",%3$s VARCHAR(255)" +                                  // VALUE
                    ",%4$s INTEGER" +                                       //PREVIOUS_ID
                    ",%5$s INTEGER" +                                       //NEXT_ID
                    ")"
            , ITEMS_TABLE
            , _ID
            , VALUE
            , PREVIOUS_ID
            , NEXT_ID
    );

}

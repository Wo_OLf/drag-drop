package com.woolf.dragdrop.bd;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.woolf.dragdrop.data.DragAndDropData;

import java.util.ArrayList;

/**
 * Created by WoOLf on 21.01.2016.
 */
public class ItemDatabaseHelper extends SQLiteOpenHelper implements DatabaseKeys {

    private static final String DATABASE_NAME = "items.db";
    private static final int DATABASE_VERSION = 1;

    private static ItemDatabaseHelper mInstance;

    public static ItemDatabaseHelper getInstance(Context context) {
        if (mInstance == null) {
            mInstance = new ItemDatabaseHelper(context);
        }
        return mInstance;
    }

    private ItemDatabaseHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {
        createItemsTable(sqLiteDatabase);
    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) {

    }

    private void createItemsTable(SQLiteDatabase db) {
        db.execSQL(CREATE_ITEMS_TABLE);
    }

    public int getHeadRecord() {
        return ItemDatabaseManager.getIdFirsRecord(mInstance);
    }

    public boolean fillDatabase(){
        return ItemDatabaseManager.fillDatabase(mInstance);
    }

    public ArrayList<DragAndDropData> loadFromDatabase(int firstId){
        return ItemDatabaseManager.fillListFromDataBase(firstId, mInstance);
    }

    public boolean updateDatabase(int fromId, int toId, boolean isMovedUp){
        return ItemDatabaseManager.updateDatabase(fromId, toId, isMovedUp, mInstance);
    }



    public static void destroy(){
        mInstance = null;
    }


}

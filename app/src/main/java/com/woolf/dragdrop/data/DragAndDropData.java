package com.woolf.dragdrop.data;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by WoOLf on 23.01.2016.
 */
public class DragAndDropData implements Parcelable {
    private int mId;
    private String mValue;

    public DragAndDropData(int id, String value) {
        mId = id;
        mValue = value;
    }


    public int getId() {
        return mId;
    }

    public void setId(int id) {
        mId = id;
    }

    public String getValue() {
        return mValue;
    }

    public void setValue(String value) {
        mValue = value;
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(this.mId);
        dest.writeString(this.mValue);
    }

    protected DragAndDropData(Parcel in) {
        this.mId = in.readInt();
        this.mValue = in.readString();
    }

    public static final Parcelable.Creator<DragAndDropData> CREATOR = new Parcelable.Creator<DragAndDropData>() {
        public DragAndDropData createFromParcel(Parcel source) {
            return new DragAndDropData(source);
        }

        public DragAndDropData[] newArray(int size) {
            return new DragAndDropData[size];
        }
    };
}

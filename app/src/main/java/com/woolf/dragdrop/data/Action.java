package com.woolf.dragdrop.data;

/**
 * Created by WoOLf on 25.01.2016.
 */
public class Action {

    public final static String BROADCAST_ACTION = "com.woolf.dragdrop.brodcast_action";

    public static final String KEY_ACTION = "Action.KEY_ACTION";
    public static final String KEY_FIRST_ID = "Action.KEY_FIRST_ID";
    public static final String KEY_STATUS = "Action.KEY_STATUS";
    public static final String KEY_LIST = "Action.KEY_LIST";

    public static final String KEY_FROM_ID = "Action.KEY_FROM_ID";
    public static final String KEY_TO_ID = "Action.KEY_TO_ID";
    public static final String KEY_IS_MOVED_UP = "Action.KEY_IS_MOVED_UP";


    public static final int LOAD_FROM_DB = 0;
    public static final int FILL_DB = 1;
    public static final int UPDATE_DB = 2;
    public static final int GET_FIRST_RECORD_FROM_DB = 3;

}

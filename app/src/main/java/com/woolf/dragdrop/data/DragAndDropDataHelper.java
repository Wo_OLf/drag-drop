package com.woolf.dragdrop.data;

/**
 * Created by WoOLf on 24.01.2016.
 */
public class DragAndDropDataHelper {

    private DragAndDropData mData;
    private int mPreviousId;
    private int mNextId;

    public DragAndDropDataHelper(DragAndDropData data, int previousId, int nextId) {
        mData = data;
        mPreviousId = previousId;
        mNextId = nextId;
    }

    public DragAndDropData getData() {
        return mData;
    }

    public void setData(DragAndDropData data) {
        mData = data;
    }

    public int getPreviousId() {
        return mPreviousId;
    }

    public void setPreviousId(int previousId) {
        mPreviousId = previousId;
    }

    public int getNextId() {
        return mNextId;
    }

    public void setNextId(int nextId) {
        mNextId = nextId;
    }
}

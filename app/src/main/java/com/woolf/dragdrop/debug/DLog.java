package com.woolf.dragdrop.debug;

import android.util.Log;

/**
 * Created by WoOLf on 22.01.2016.
 */
public class DLog {

    private static final String TAG = "D&D";

    public static void i(String value) {
        Log.i(TAG, value);
    }


    public static void i(int value) {
        Log.i(TAG, Integer.toString(value));
    }

    public static void i(long value) {
        Log.i(TAG, Long.toString(value));
    }
}

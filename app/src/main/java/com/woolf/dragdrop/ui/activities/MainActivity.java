package com.woolf.dragdrop.ui.activities;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.drawable.NinePatchDrawable;
import android.os.Build;
import android.os.Bundle;
import android.os.PersistableBundle;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.h6ah4i.android.widget.advrecyclerview.animator.GeneralItemAnimator;
import com.h6ah4i.android.widget.advrecyclerview.animator.RefactoredDefaultItemAnimator;
import com.h6ah4i.android.widget.advrecyclerview.decoration.ItemShadowDecorator;
import com.h6ah4i.android.widget.advrecyclerview.decoration.SimpleListDividerDecorator;
import com.h6ah4i.android.widget.advrecyclerview.draggable.RecyclerViewDragDropManager;
import com.woolf.dragdrop.DragAndDropApplication;
import com.woolf.dragdrop.R;
import com.woolf.dragdrop.bd.DatabaseKeys;
import com.woolf.dragdrop.data.Action;
import com.woolf.dragdrop.data.DragAndDropData;
import com.woolf.dragdrop.service.WorkWitchDatabaseService;
import com.woolf.dragdrop.ui.adapters.MyDraggableItemAdapter;

import java.util.ArrayList;

/**
 * Created by WoOLf on 22.01.2016.
 */
public class MainActivity extends AppCompatActivity implements DatabaseKeys {

    private static final String DATA = "MainActivity.DATA";

    BroadcastReceiver mBroadcastReceiver;


    private RecyclerView mRecyclerView;
    private ProgressBar mProgressBar;

    private RecyclerView.LayoutManager mLayoutManager;
    private MyDraggableItemAdapter mAdapter;
    private RecyclerView.Adapter mWrappedAdapter;
    private RecyclerViewDragDropManager mRecyclerViewDragDropManager;


    private ArrayList<DragAndDropData> mItemsList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        initView();
        initAdapter();
        initBroadcastReceiver();
        if (savedInstanceState != null) {
            mItemsList = savedInstanceState.getParcelableArrayList(DATA);
            successLoad(mItemsList);
        } else {
            startLoading();
        }
    }

    @Override
    public void onSaveInstanceState(Bundle outState, PersistableBundle outPersistentState) {
        outState.putParcelableArrayList(DATA, mItemsList);
        super.onSaveInstanceState(outState, outPersistentState);
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        mItemsList = savedInstanceState.getParcelableArrayList(DATA);
        super.onRestoreInstanceState(savedInstanceState);

    }

    private void startLoading() {
        mProgressBar.setVisibility(View.VISIBLE);
        Intent startService = new Intent(DragAndDropApplication.APP_CONTEXT, WorkWitchDatabaseService.class);
        startService.putExtra(Action.KEY_ACTION, Action.GET_FIRST_RECORD_FROM_DB);
        startService(startService);
    }


    private void fillBd(int firstId) {
        Intent startService = new Intent(DragAndDropApplication.APP_CONTEXT, WorkWitchDatabaseService.class);
        startService.putExtra(Action.KEY_ACTION, Action.LOAD_FROM_DB);
        startService.putExtra(Action.KEY_FIRST_ID, firstId);

        startService(startService);
    }

    private void actionGetFirstRecordFromDb(Intent intent) {
        int firstId = intent.getExtras().getInt(Action.KEY_FIRST_ID);
        if (firstId == 0) {
            fillDataBase();
        } else {
            fillBd(firstId);
        }
    }

    private void actionFillDb(Intent intent) {
        boolean isSuccess = intent.getExtras().getBoolean(Action.KEY_STATUS);
        if (isSuccess) {
            startLoading();
        } else {
            Toast.makeText(DragAndDropApplication.APP_CONTEXT, R.string.error_fill_db, Toast.LENGTH_SHORT).show();
        }
    }

    private void actionUpdateDb(Intent intent) {
        boolean isSuccess = intent.getExtras().getBoolean(Action.KEY_STATUS);
        if (!isSuccess) {
            Toast.makeText(DragAndDropApplication.APP_CONTEXT, R.string.error_update_db, Toast.LENGTH_SHORT).show();
            startLoading();
        }
    }

    private void actionLoadFromDb(Intent intent) {
        mItemsList = intent.getParcelableArrayListExtra(Action.KEY_LIST);
        successLoad(mItemsList);
    }

    private void successLoad(ArrayList<DragAndDropData> list) {
        mAdapter.setData(list);
        if (list != null && !list.isEmpty()) {
            mProgressBar.setVisibility(View.GONE);
        }
    }

    private void initView() {
        mRecyclerView = (RecyclerView) findViewById(R.id.rv_main);
        mProgressBar = (ProgressBar) findViewById(R.id.pb_main);
    }

    private void initBroadcastReceiver() {
        mBroadcastReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                int action = intent.getExtras().getInt(Action.KEY_ACTION);
                switch (action) {
                    case Action.GET_FIRST_RECORD_FROM_DB:
                        actionGetFirstRecordFromDb(intent);
                        break;
                    case Action.LOAD_FROM_DB:
                        actionLoadFromDb(intent);
                        break;

                    case Action.FILL_DB:
                        actionFillDb(intent);
                        break;

                    case Action.UPDATE_DB:
                        actionUpdateDb(intent);
                        break;
                }

            }
        };
        IntentFilter filter = new IntentFilter(Action.BROADCAST_ACTION);
        registerReceiver(mBroadcastReceiver, filter);
    }

    private void initAdapter() {
        mLayoutManager = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);
        // drag & drop manager
        mRecyclerViewDragDropManager = new RecyclerViewDragDropManager();
        mRecyclerViewDragDropManager.setDraggingItemShadowDrawable(
                (NinePatchDrawable) ContextCompat.getDrawable(this, R.drawable.material_shadow_z3));
        // Start dragging after long press
        mRecyclerViewDragDropManager.setInitiateOnLongPress(true);
        mRecyclerViewDragDropManager.setInitiateOnMove(false);
        //adapter
        mAdapter = new MyDraggableItemAdapter();

        mWrappedAdapter = mRecyclerViewDragDropManager.createWrappedAdapter(mAdapter);      // wrap for dragging

        final GeneralItemAnimator animator = new RefactoredDefaultItemAnimator();

        mRecyclerView.setLayoutManager(mLayoutManager);
        mRecyclerView.setAdapter(mWrappedAdapter);  // requires *wrapped* adapter
        mRecyclerView.setItemAnimator(animator);

        // additional decorations
        //noinspection StatementWithEmptyBody
        if (supportsViewElevation()) {
            // Lollipop or later has native drop shadow feature. ItemShadowDecorator is not required.
        } else {
            mRecyclerView.addItemDecoration(new ItemShadowDecorator((NinePatchDrawable) ContextCompat.getDrawable(this, R.drawable.material_shadow_z1)));
        }
        mRecyclerView.addItemDecoration(new SimpleListDividerDecorator(ContextCompat.getDrawable(this, R.drawable.list_divider_h), true));

        mRecyclerViewDragDropManager.attachRecyclerView(mRecyclerView);
    }

    private void fillDataBase() {
        Intent startService = new Intent(DragAndDropApplication.APP_CONTEXT, WorkWitchDatabaseService.class);
        startService.putExtra(Action.KEY_ACTION, Action.FILL_DB);
        startService(startService);
    }


    @Override
    protected void onDestroy() {
        super.onDestroy();
        unregisterReceiver(mBroadcastReceiver);
    }

    @Override
    protected void onPause() {
        if (mRecyclerViewDragDropManager != null) {
            mRecyclerViewDragDropManager.cancelDrag();
        }
        super.onPause();
    }


    private boolean supportsViewElevation() {
        return (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP);
    }


}




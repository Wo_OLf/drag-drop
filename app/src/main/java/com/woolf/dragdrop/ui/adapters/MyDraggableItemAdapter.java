package com.woolf.dragdrop.ui.adapters;


import android.content.Intent;
import android.support.v4.view.ViewCompat;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.TextView;

import com.h6ah4i.android.widget.advrecyclerview.draggable.DraggableItemAdapter;
import com.h6ah4i.android.widget.advrecyclerview.draggable.DraggableItemConstants;
import com.h6ah4i.android.widget.advrecyclerview.draggable.ItemDraggableRange;
import com.h6ah4i.android.widget.advrecyclerview.utils.AbstractDraggableItemViewHolder;
import com.woolf.dragdrop.DragAndDropApplication;
import com.woolf.dragdrop.R;
import com.woolf.dragdrop.data.Action;
import com.woolf.dragdrop.data.DragAndDropData;
import com.woolf.dragdrop.service.WorkWitchDatabaseService;
import com.woolf.dragdrop.utils.DrawableUtils;
import com.woolf.dragdrop.utils.ViewUtils;

import java.util.ArrayList;

public class MyDraggableItemAdapter extends RecyclerView.Adapter<MyDraggableItemAdapter.MyViewHolder>
        implements DraggableItemAdapter<MyDraggableItemAdapter.MyViewHolder> {

    private interface Draggable extends DraggableItemConstants {
    }

    private ArrayList<DragAndDropData> mData;


    public static class MyViewHolder extends AbstractDraggableItemViewHolder {
        public FrameLayout mContainer;
        public View mDragHandle;
        public TextView mTextView;

        public MyViewHolder(View v) {
            super(v);
            mContainer = (FrameLayout) v.findViewById(R.id.container);
            mDragHandle = v.findViewById(R.id.drag_handle);
            mTextView = (TextView) v.findViewById(android.R.id.text1);
        }
    }

    public MyDraggableItemAdapter(ArrayList<DragAndDropData> data) {
        mData = data;
        setHasStableIds(true);
    }

    public MyDraggableItemAdapter() {

        setHasStableIds(true);
    }

    public void setData(ArrayList<DragAndDropData> data) {
        mData = data;
        notifyDataSetChanged();
    }

    @Override
    public long getItemId(int position) {
        return mData == null ? 0 : mData.get(position).getId();
    }

    @Override
    public int getItemViewType(int position) {
        return 0;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        final LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        final View v = inflater.inflate(R.layout.row_drag_and_drop, parent, false);
        return new MyViewHolder(v);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        DragAndDropData data = mData.get(position);

        holder.mTextView.setText(data.getValue());

        // set background resource (target view ID: container)
        final int dragState = holder.getDragStateFlags();

        if (((dragState & Draggable.STATE_FLAG_IS_UPDATED) != 0)) {
            int bgResId;

            if ((dragState & Draggable.STATE_FLAG_IS_ACTIVE) != 0) {
                bgResId = R.drawable.bg_item_dragging_active_state;

                // need to clear drawable state here to get correct appearance of the dragging item.
                DrawableUtils.clearState(holder.mContainer.getForeground());
            } else if ((dragState & Draggable.STATE_FLAG_DRAGGING) != 0) {
                bgResId = R.drawable.bg_item_dragging_state;
            } else {
                bgResId = R.drawable.bg_item_normal_state;
            }

            holder.mContainer.setBackgroundResource(bgResId);
        }
    }

    @Override
    public int getItemCount() {
        return mData == null ? 0 : mData.size();
    }

    @Override
    public void onMoveItem(int fromPosition, int toPosition) {
        if (fromPosition == toPosition) {
            return;
        }
        DragAndDropData oldData = mData.get(fromPosition);
        DragAndDropData newData = mData.get(toPosition);
        updateItemInDatabase(oldData.getId(), newData.getId(), fromPosition > toPosition);
        moveItem(fromPosition, toPosition);
        notifyItemMoved(fromPosition, toPosition);
    }

    private void updateItemInDatabase(int fromId, int toId, boolean isMovedUp) {
        Intent intent = new Intent(DragAndDropApplication.APP_CONTEXT, WorkWitchDatabaseService.class);
        intent.putExtra(Action.KEY_ACTION, Action.UPDATE_DB);
        intent.putExtra(Action.KEY_FROM_ID, fromId);
        intent.putExtra(Action.KEY_TO_ID, toId);
        intent.putExtra(Action.KEY_IS_MOVED_UP, isMovedUp);
        DragAndDropApplication.APP_CONTEXT.startService(intent);
    }


    private void moveItem(int fromPosition, int toPosition) {
        if (fromPosition == toPosition) {
            return;
        }
        DragAndDropData item = mData.remove(fromPosition);
        mData.add(toPosition, item);
    }

    @Override
    public boolean onCheckCanStartDrag(MyViewHolder holder, int position, int x, int y) {

        // x, y --- relative from the itemView's top-left
        final View containerView = holder.mContainer;
        final View dragHandleView = holder.mDragHandle;

        final int offsetX = containerView.getLeft() + (int) (ViewCompat.getTranslationX(containerView) + 0.5f);
        final int offsetY = containerView.getTop() + (int) (ViewCompat.getTranslationY(containerView) + 0.5f);

        return ViewUtils.hitTest(dragHandleView, x - offsetX, y - offsetY);
    }

    @Override
    public ItemDraggableRange onGetItemDraggableRange(MyViewHolder holder, int position) {
        // no drag-sortable range specified
        return null;
    }
}
package com.woolf.dragdrop;

import android.app.Application;
import android.content.Context;

/**
 * Created by WoOLf on 22.01.2016.
 */
public class DragAndDropApplication extends Application {

    public static Context APP_CONTEXT;

    @Override
    public void onCreate() {
        super.onCreate();
        APP_CONTEXT = this;
    }
}
